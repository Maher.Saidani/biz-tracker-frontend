export interface User {
    id?: number;
    name: string;
    email: string;
    password: string;
    type?: string;
    date?: Date;
    online?: boolean;
}