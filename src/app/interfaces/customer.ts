export interface Customer {
  id: string;
  name: string;
  addressline1: string;
  addressline2: string;
  addressCity: string;
  addressZIPCode: string;
  addressCountry: string;
  phone: string;
  accountOwner: string;
  orders: number[];
  longitude: number;
  latitude: number;
}