import { Country } from './country';

export const DUMMYCOUNTRIES: Country[] = [
    {
        "name": "China",
        "series": [
          {
            "name": "2018",
            "value": 2243772
          },
          {
            "name": "2017",
            "value": 1227770
          }
        ]
      },
      {
        "name": "USA",
        "series": [
          {
            "name": "2018",
            "value": 1126000
          },
          {
            "name": "2017",
            "value": 764666
          }
        ]
      },
      {
        "name": "Norway",
        "series": [
          {
            "name": "2018",
            "value": 296215
          },
          {
            "name": "2017",
            "value": 209122
          }
        ]
      },
      {
        "name": "Japan",
        "series": [
          {
            "name": "2018",
            "value": 257363
          },
          {
            "name": "2017",
            "value": 205350
          }
        ]
      },
      {
        "name": "Germany",
        "series": [
          {
            "name": "2018",
            "value": 196750
          },
          {
            "name": "2017",
            "value": 129246
          }
        ]
      },
      {
        "name": "France",
        "series": [
          {
            "name": "2018",
            "value": 204617
          },
          {
            "name": "2017",
            "value": 149797
          }
        ]
      }
];