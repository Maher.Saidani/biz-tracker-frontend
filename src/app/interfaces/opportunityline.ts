export interface OpportunityLine {
  id: string;
  opportunityId: string;
  name: string;
  description: string;
  productCode: string;
  quantity: number;
  totalPrice: number;
  unitPrice: number;
}