import { Stat } from './stat';

export interface Country {
    name: string;
    series: Stat[];
}