import { OpportunityLine } from './opportunityline'

export interface Opportunity {
  accountId: string;
  amount: number;
  closeDate: string;
  createdDate: string;
  id: string;
  won: boolean;
  products: OpportunityLine [];
}