import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  registerURL = environment.hostedUrl + '/newUser';
  // registerURL = environment.apiUrl + '/newUser';

  addNewUser(newUser: User): Observable<number>{
    console.log('register Service: ' + newUser);
    return this.http.post<number>(this.registerURL, newUser);
  }

}
