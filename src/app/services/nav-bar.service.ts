import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {

  constructor(private http: HttpClient) { }

  // getUserURL = environment.apiUrl + '/home/';
  getUserURL = environment.hostedUrl + '/home/';

  getLoggedUser(userId: number): Observable<User> {
    console.log('navbar service: ' + userId);
    return this.http.get<User>(this.getUserURL + userId);
  }
}
