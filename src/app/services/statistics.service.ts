import { Injectable } from '@angular/core';
import { Opportunity } from '../interfaces/opportunity';
import { Country } from '../interfaces/country';
import {Customer } from '../interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor() { }

  getOpportunitiesYTD(orders: Opportunity[], filter?: Customer): Country[] {
    const result: Country[] = [];

    for (let i = 0; i < (new Date().getMonth() + 1); i++) {
      const monthlyData: Country = {name: '', series: []};
      monthlyData.name = ('0' + (i + 1)).slice(-2);
      for (const opportunity of orders) {
        if (!filter || filter.id === opportunity.accountId) {
          if (opportunity.closeDate.substr(5, 2) === ('0' + (i + 1)).slice(-2)) {
            if (monthlyData.series.some(e => e.name === opportunity.closeDate.substr(0, 4))) {
              monthlyData.series.find(e => e.name === opportunity.closeDate.substr(0, 4)).value += opportunity.amount;
            } else {
              monthlyData.series.push({name: opportunity.closeDate.substr(0, 4), value: opportunity.amount})
            }
          }
        }
      }
      result.push(monthlyData);
    }
    return result;
  }

  getForecast(orders: Opportunity[]): string {
    let result = 0;
    for (const opportunity of orders) {
      result += opportunity.amount;
    }
    return this.stringify(result);
  }

  getTotalSalesPerMonth(orders: Opportunity[]): string {
    const date = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2);
    let result = 0;
    for (const opportunity of orders) {
      if (opportunity.closeDate.substr(0, 7) === date) {
        result += opportunity.amount;
      }
    }
    return this.stringify(result);
  }

    getTotalItemsPerMonth(orders: Opportunity[]): string {
    const date = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2);
    let result = 0;
    for (const opportunity of orders) {
      if (opportunity.closeDate.substr(0, 7) === date) {
        for (const line of opportunity.products) {
          result += line.quantity;
        }
      }
    }
    return this.stringify(result);
  }


  stringify(myNumber: number): string {
    let result: string;
    if (myNumber > 1_000_000) {
      myNumber /= 1_000_000;
      result = myNumber + 'M';
    } else if (myNumber > 1000) {
      myNumber /= 1000;
      result = myNumber + 'k';
    } else if (myNumber === 0) {
      result = '-';
    } else {
      result = myNumber.toString();
    }
    return result;
  }
}
