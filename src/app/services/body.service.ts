import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../interfaces/customer';
import * as mapboxgl from 'mapbox-gl';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/country';
import { Opportunity } from '../interfaces/opportunity';



@Injectable({
  providedIn: 'root'
})
export class BodyService {

  /*
  getNumbersURL = environment.apiUrl + '/getNumbers';
  customerURL = environment.apiUrl + '/getCustomers';
  countriesURL = environment.apiUrl + '/getCountries';
  dataURL = environment.apiUrl + '/getOpportunities';
  */

  getNumbersURL = environment.hostedUrl + '/getNumbers';
  customerURL = environment.hostedUrl + '/getCustomers';
  countriesURL = environment.hostedUrl + '/getCountries';
  dataURL = environment.hostedUrl + '/getOpportunities';


  constructor(private http: HttpClient) {
        (mapboxgl as any).accessToken = environment.mapbox.accessToken;
   }

  getNumbers(): Observable<string[]> {
    return this.http.get<string[]>(this.getNumbersURL);
  }

  getCustomers(): Observable<Customer[]>  {
    return this.http.get<Customer[]>(this.customerURL);
  }

  getCountries(): Observable<Country[]>  {
    return this.http.get<Country[]>(this.countriesURL);
  }

  getOpportunities(): Observable<Opportunity[]> {
    return this.http.get<Opportunity[]>(this.dataURL);
  }

}
