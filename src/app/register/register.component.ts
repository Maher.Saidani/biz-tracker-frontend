import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { RegisterService } from '../services/register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  newUser : User = {name: "", password: "", email: ""};


  constructor(
    private registerService: RegisterService,
    private router: Router,
    ) { }

  ngOnInit() {
  }

  addNewUSer(): void {
    console.log("register component: " + this.newUser);
    this.registerService.addNewUser(this.newUser).subscribe(
      result => {
        console.log(result);
        if(result===-1){
          
        }
        else{this.newUser.id = result;}
        this.router.navigate(['/shared/register']);
      }
    );
  }


}
