import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterComponent } from './register/register.component';
import { SharedMessageComponent } from './shared-message/shared-message.component'

const routes: Routes = [
  { path:'dashboard', component: BodyComponent},
  { path:'body', component: BodyComponent},
  { path:'register', component: RegisterComponent },
  { path:'login', component: LoginComponent },
  { path:'home', component: HomePageComponent },
  { path:'shared/:key', component: SharedMessageComponent },
  { path:'', component: LoginComponent },
  { path: '**', component: SharedMessageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
