import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NavBarService } from '../services/nav-bar.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {


  user: User = {name: '', password: '', email: ''};

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private navBarService: NavBarService
  ) { }

  ngOnInit() {
    this.CheckUser();
    this.getLoggedUser();
  }


  getLoggedUser(): void {
    this.navBarService.getLoggedUser(this.user.id).subscribe(
      (returnUser) => {
        console.log('navbar comp: ' + returnUser);
        this.user = returnUser;
        console.log(this.user);
        if (returnUser === null ) {
          console.log('error');
          this.router.navigate(['/shared/404']);
        }
      }
    );
  }

  logout(): void {
    this.cookieService.deleteAll();
    this.router.navigate(['/shared/logout']);
  }

  CheckUser(): void {
    const id = this.cookieService.get('LoginData');
    if (id == null) {
      this.cookieService.delete('LoginData');
      this.router.navigate(['/shared/logout']);
    } else {
      this.user.id = parseInt(id);
    }
  }
}
