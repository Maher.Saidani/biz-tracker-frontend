import { Component, OnInit } from '@angular/core';
import { BodyService } from '../services/body.service';
import { StatisticsService } from '../services/statistics.service';
import { Customer } from '../interfaces/customer';
import { Country } from '../interfaces/country';
import { Opportunity } from '../interfaces/opportunity';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  selectedCustomer: Customer;
  data: Opportunity [];

  // Data-related variables
  customers: Customer[];
  dataLoaded: Promise<boolean>;

  // Headlines variables
  titleTotalSales = 'Total Sales / month';
  numbersTotalSales: string;
  titleForecast = 'Forecast level';
  numbersForecast: string;
  titleTotalItems = 'Items sold / month';
  numbersTotalItems: string;

  // Charts variables
  titleOpportunitiesYTD = 'Closed opportunities YTD';
  opportunitiesYTD: Country[];
  chartTitle2 = 'Second Graph';
  chartTitle3 = 'Third Graph';

  constructor(
    private bodyService: BodyService,
    private statService: StatisticsService
    ) { }

  ngOnInit() {
    this.gatherInformations();
  }

  gatherInformations(): void {

    forkJoin([this.bodyService.getCustomers(), this.bodyService.getOpportunities()]).subscribe(results => {
      this.customers = results[0];
      this.data = results[1];
      this.updateData();
      this.dataLoaded = Promise.resolve(true);
    })
  }

  updateData(): void {
      this.numbersTotalSales = this.statService.getTotalSalesPerMonth(this.data);
      this.numbersForecast = this.statService.getForecast(this.data);
      this.numbersTotalItems = this.statService.getTotalItemsPerMonth(this.data);
      this.opportunitiesYTD = this.statService.getOpportunitiesYTD(this.data);
  }

  customerChangedHandler(customer: Customer): void {
    if (this.selectedCustomer === customer) {
      this.selectedCustomer = null;
      this.opportunitiesYTD = this.statService.getOpportunitiesYTD(this.data);
    } else {
      this.selectedCustomer = customer;
      this.opportunitiesYTD = this.statService.getOpportunitiesYTD(this.data, customer);
    }
  }
}