import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../../interfaces/customer';
import * as mapboxgl from 'mapbox-gl';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  // Map configuration
  map: mapboxgl.Map;
  style = 'mapbox://styles/awproject/ckbkjgdze0tb21iqr4ti33ho0';
  lat = 37.75;
  lng = -122.44;
  message = 'Hello World !';

  // Class attributes
  @Output() selectedCustomer: EventEmitter<Customer> =   new EventEmitter();
  @Input() customers: Customer[];


  constructor() { }

  ngOnInit() {
    this.initializeMap();
  }

  initializeMap() {

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;

        this.map.flyTo({
          center: [this.lng, this.lat]
        });
      });
    }

    this.buildMap();
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 13,
      center: [this.lng, this.lat]
    });
    for (const customer of this.customers) {
      const popup = new mapboxgl.Popup({ offset: 25 })
        .setHTML('<strong>' + customer.name + '</strong><br/>' + customer.phone);
      new mapboxgl.Marker()
        .setLngLat([customer.longitude, customer.latitude])
        .setPopup(popup)
        .addTo(this.map).getElement().addEventListener('click', () => {
      console.log(customer.name + 'selected.');
      this.selectedCustomer.emit(customer);
      });
    }

  }
}
