import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.css']
})
export class HeadlineComponent implements OnInit {
  @Input() title: string;
  @Input() number: number;

  constructor() { }

  ngOnInit() {
  }

}
