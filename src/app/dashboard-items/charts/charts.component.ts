import { Component, OnInit, Input } from '@angular/core';
import { Country } from 'src/app/interfaces/country';
import { DUMMYCOUNTRIES } from 'src/app/interfaces/mockCountries';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  view: any[] = [600, 400];
  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Sales';
  timeline = true;

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };
  showLabels = true;

  @Input() title: string;
  @Input() countries: Country[];

  constructor() { }

  ngOnInit() {
    console.log('Charts: ');
    console.log(this.countries);
  }

}
