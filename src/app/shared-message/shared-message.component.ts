import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-shared-message',
  templateUrl: './shared-message.component.html',
  styleUrls: ['./shared-message.component.css']
})
export class SharedMessageComponent implements OnInit {

  successMessage: String;
  errorMessage: String;

  constructor(
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.setMessage(params.key);
      }
    );
  }

  setMessage(key: string): void {
    if(key === "logout"){
      this.successMessage = "Logout Success!";
    }
    else if (key === "register"){
      this.successMessage = "Registration Success!";
    }
    else{
      this.errorMessage = "404 Error: Page not found !";
    }
  }

}
