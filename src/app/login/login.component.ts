import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { LoginService } from '../services/login.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  user: User = {name: '', password: '', email: ''};
  loginfailed = false;

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.checkUser();
  }

  checkUser(): void {
    const cookieValue = this.cookieService.get('LoginData');
    if (cookieValue !== '') {
      console.log('Coucou |' + cookieValue + '|');
      this.router.navigate(['/home/']);
    }
  }

  login(): void {
    this.loginService.login(this.user).subscribe(
      (returnUser) => {
        if (returnUser != null) {
          this.user = returnUser;
          console.log(returnUser);
          const dateNow = new Date();
          dateNow.setMinutes(dateNow.getMinutes() + 10);
          this.cookieService.set('LoginData', returnUser.id.toString(), dateNow);
          this.router.navigate(['/home/']);
        } else {
          this.loginfailed = true;
        }
      }
    );
  }
}
